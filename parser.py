#!/usr/bin/python 
# -*- coding: utf-8 -*-
#
# Parser https://otvet.mail.ru
# Автор: Ракитин Виталий
#

import requests
import re

def download(num):
    question = None
    answers = None
    try:
        request = requests.get('https://otvet.mail.ru/question/' + str(num))
        question = re.findall(u'<title>Ответы@Mail.Ru: (.*?)</title>', request.text)[0]
        answers = re.findall(u'<div class="a--atext atext" itemprop="text">(.*?)</div>', request.text)
        answers = map(lambda x: re.sub(u'<br><br>','\n', x), answers)
        answers = map(lambda x: re.sub(u'<.*>','\n', x), answers)
    except:
        pass #Page does not exist
    return question, answers


if __name__ == '__main__':
    for i in xrange(100):
        q,a = download(i)
        if q != None and a != None:
            print q
            print a[0]